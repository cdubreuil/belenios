# Description du projet

L'objectif de ce projet est d'automatiser entierrement le déploiement d'un serveur d'élection.

Chaque script d'installation crée une image docker/podman dédiée


# contenu des images Docker / padman créés 


Installation sur une debian 10
Installation de l'ensemble des paquets necessaires
Création d'un compte usager dédié (belenios)
Récupération de la branche git et complilation d'ocam
Build du serveur belenios
Personnalisation au couleurs du GIP , et mise en place du logo
Création d'un script de démarrage
Installation de supervisord pour gérer le démarrage du service

# Installation
récupérer le projet

git clone https://gitlab.adullact.net/cdubreuil/belenios

Puis, depuis l'interrieur du dossier 

./install_belenios.sh -->  installation de belenios
./install_belenios_swergas --> installation de belenios_swergas
