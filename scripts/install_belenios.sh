#!/bin/bash
git clone https://gitlab.inria.fr/belenios/belenios/
cp -a adaptation_recia belenios/ 
cd belenios
cat ../podman/Dockerfile_belenios | docker build -t recia/belenios -f - .
