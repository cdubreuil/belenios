#!/bin/bash
git clone https://gitlab.inria.fr/belenios-forks/belenios-swergas
cp -r adaptation_recia belenios-swergas/
cd belenios-swergas
cat ../podman/Dockerfile_belenios_swergas | docker build -t recia/belenios_swergas -f - .
